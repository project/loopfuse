Module: LoopFuse Statistics
Author: Ethan Fremen <i@mindlace.net>


Description
===========
Adds the LoopFuse Statistics tracking system to your website.

Requirements
============

* LoopFuse user account


Installation
============
* Copy the 'loopfusestatistics' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your LoopFuse Statistics account number.

You can also track the username and/or user ID who visits each page.
This data will be visible in LoopFuse Statistics as segmentation data.
If you enable the profile.module you can also add more detailed
information about each user to the segmentation tracking.

All pages will now have the required JavaScript added to the
HTML footer can confirm this by viewing the page source from
your browser.

Advanced Settings
=================

To speed up page loading you may also cache the LoopFuse listen.js
file locally. You need to make sure the site file system is in public
download mode.